require 'rails_helper'

RSpec.describe "Potepan::Api::Suggests", type: :request do
  describe "GET /potepan/api/suggests" do
    let(:suggests) { JSON.parse(response.body) }

    before do
      [
        'apache',
        'apache for women',
        'apache for men',
        'bag',
        'bag for women',
        'bag for men',
        'mug',
        'shirt',
        'shirt for women',
        'shirt for men',
        't-shirt',
        'ruby',
        'ruby for women',
        'ruby for men',
        'rails',
        'rails for women',
        'rails for men',
        'RUBY ON RAILS',
        'RUBY ON RAILS bag',
        'RUBY ON RAILS t-shirt',
        'TOTE',
      ].each do |keyword|
        create(:suggest, keyword: keyword)
      end

      get potepan_api_suggests_path, params: params, headers: headers
    end

    context "with headers" do
      let(:headers) { { 'Authorization': "Bearer #{ENV['SUGGEST_API_KEY']}" } }

      context "and with query" do
        context "keyword = r and max_num = 5" do
          let(:params) do
            {
              keyword: 'r',
              max_num: 5,
            }
          end

          it "returns 5 words capital letter 'r'" do
            expect(suggests).to contain_exactly(
              'ruby',
              'ruby for women',
              'ruby for men',
              'rails',
              'rails for women',
            )
          end
        end

        context "keyword = r" do
          let(:params) { { keyword: 'r' } }

          it "returns all words capital letter 'r'" do
            expect(suggests).to contain_exactly(
              'ruby',
              'ruby for women',
              'ruby for men',
              'rails',
              'rails for women',
              'rails for men',
              'RUBY ON RAILS',
              'RUBY ON RAILS bag',
              'RUBY ON RAILS t-shirt',
            )
          end
        end

        context "keyword=''" do
          let(:params) { { keyword: '' } }

          it "returns all suggest words" do
            expect(suggests).to contain_exactly(
              'apache',
              'apache for women',
              'apache for men',
              'bag',
              'bag for women',
              'bag for men',
              'mug',
              'shirt',
              'shirt for women',
              'shirt for men',
              't-shirt',
              'ruby',
              'ruby for women',
              'ruby for men',
              'rails',
              'rails for women',
              'rails for men',
              'RUBY ON RAILS',
              'RUBY ON RAILS bag',
              'RUBY ON RAILS t-shirt',
              'TOTE'
            )
          end
        end
      end

      context "and without query" do
        let(:params) { {} }

        it "returns a 400 response" do
          expect(response).to have_http_status(400)
        end
      end
    end

    context "without headers" do
      let(:headers) { {} }
      let(:params) do
        {
          keyword: 'r',
          max_num: 5,
        }
      end

      it "returns a 401 response" do
        expect(response).to have_http_status(401)
      end
    end
  end
end
