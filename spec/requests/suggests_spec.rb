require 'rails_helper'

RSpec.describe "Suggests", type: :request do
  describe "GET /potepan/suggests" do
    let(:suggests) { JSON.parse(response.body) }

    before { get potepan_suggests_path, params: params }

    context "with a query" do
      let(:params) do
        {
          keyword: keyword,
          max_num: 5,
        }
      end

      context "when the query is keyword=r", vcr: { cassette_name: 'suggests_r' } do
        let(:keyword) { 'r' }

        it "returns a 200 response" do
          expect(response).to have_http_status(200)
        end

        it "returns array json with capital letter 'r'" do
          suggests.each { |suggest| expect(suggest[0]).to eq 'r' }
        end

        it "returns array json with 5 elements" do
          expect(suggests.length).to eq 5
        end
      end

      context "when the query is keyword=x", vcr: { cassette_name: 'suggests_x' } do
        let(:keyword) { 'x' }

        it "returns a 200 response" do
          expect(response).to have_http_status(200)
        end

        it "returns array json without elements" do
          expect(suggests.length).to eq 0
        end
      end
    end

    context "without a query", vcr: { cassette_name: 'suggests_fail' } do
      let(:params) { {} }

      it "returns a 200 response" do
        expect(response).to have_http_status(200)
      end

      it "returns array json without elements" do
        expect(suggests.length).to eq 0
      end
    end
  end
end
