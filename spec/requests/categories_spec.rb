require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET /potepan/categories/:id" do
    context "when a taxon exist" do
      let(:taxonomy) { create(:taxonomy) }
      let(:taxon) { create(:taxon, taxonomy: taxonomy) }
      let!(:product) { create(:product, taxons: [taxon]) }

      before do
        get potepan_category_path(taxon.id)
      end

      it "returns a 200 response" do
        expect(response).to have_http_status(200)
      end

      it "has the taxon name" do
        expect(response.body).to include taxon.name
      end

      it "has the taxonomy name" do
        expect(response.body).to include taxonomy.name
      end

      it "has the product name" do
        expect(response.body).to include product.name
      end
    end
  end
end
