require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET /potepan/products/:id" do
    context "when a product with associated products exists" do
      let(:taxonomy1) { create(:taxonomy, id: 1) }
      let(:taxonomy2) { create(:taxonomy, id: 2) }
      let(:taxon1) { create(:taxon, taxonomy: taxonomy1) }
      let(:taxon2) { create(:taxon, taxonomy: taxonomy2) }
      let(:product)   { create(:product, name: "Main Product", taxons: [taxon1, taxon2]) }
      let!(:product1) { create(:product, name: "Product1", taxons: [taxon1]) }
      let!(:product2) { create(:product, name: "Product2", taxons: [taxon1]) }
      let!(:product3) { create(:product, name: "Product3", taxons: [taxon2]) }
      let!(:product4) { create(:product, name: "Product4", taxons: [taxon2]) }
      let!(:product5) { create(:product, name: "Product5", taxons: [taxon1, taxon2]) }

      before do
        get potepan_product_path(product.id)
      end

      it "returns a 200 response" do
        expect(response).to have_http_status(200)
      end

      it "has the product name" do
        expect(response.body).to include "Main Product"
      end

      it "has the 4 assoiated products name" do
        expect(response.body).to include "Product5"
        expect(response.body).to include "Product1"
        expect(response.body).to include "Product2"
        expect(response.body).to include "Product3"
        expect(response.body).not_to include "Product4"
      end
    end
  end
end
