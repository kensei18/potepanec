FactoryBot.define do
  factory :suggest, class: 'Potepan::Suggest' do
    keyword { 'keyword' }
  end
end
