require 'rails_helper'

RSpec.describe "Product", type: :system do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:product1) { create(:product, name: "Product1", taxons: [taxon]) }
  let!(:product2) { create(:product, name: "Product2", taxons: [taxon]) }
  let!(:product3) { create(:product, name: "Product3", taxons: [taxon]) }
  let!(:product4) { create(:product, name: "Product4", taxons: [taxon]) }

  it 'has a single product page working well' do
    visit potepan_product_path(product.id)

    expect(title).to have_text product.name

    within('.mainContent') do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_text    product.price
      expect(page).to have_selector '.item'
      expect(page).to have_selector '.thumb'
    end

    within('.lightSection') do
      expect(page).to have_content product.name
      click_link 'Home'
    end

    expect(title).to eq 'BIGBAG Store'

    visit potepan_product_path(product.id)

    within('.productsContent') do
      expect(page).to have_selector '.productBox', count: 4
      expect(page).to have_selector 'h5', text: 'Product1'
      expect(page).to have_selector 'h5', text: 'Product2'
      expect(page).to have_selector 'h5', text: 'Product3'
      expect(page).to have_selector 'h5', text: 'Product4'

      click_link 'Product1'
    end

    expect(title).to have_text 'Product1'
  end
end
