require 'rails_helper'

RSpec.describe "Categories", type: :system do
  let(:taxonomy1) { create(:taxonomy, name: 'Root1') }
  let!(:taxonomy2) { create(:taxonomy, name: 'Root2') }
  let(:root_taxon) { taxonomy1.root }
  let(:taxon1) { create(:taxon, name: 'Taxon1', parent: root_taxon) }
  let(:taxon2) { create(:taxon, name: 'Taxon2', parent: root_taxon) }
  let(:taxon3) { create(:taxon, name: 'Taxon3', parent: taxon2) }
  let!(:product1) { create(:product, name: 'Product1', slug: 'product1', taxons: [taxon1]) }
  let!(:product2) { create(:product, name: 'Product2', slug: 'product2', taxons: [taxon1]) }
  let!(:product3) { create(:product, name: 'Product3', slug: 'product3', taxons: [taxon3]) }

  it "has categories page working well" do
    visit potepan_category_path(root_taxon.id)

    within('.pageHeader') do
      expect(page).to have_selector 'h2', text: 'Root'
      expect(page).to have_selector 'li', text: 'Root'
    end

    expect(page).to have_selector '.productBox', count: 3
    expect(page).to have_selector 'h5', text: 'Product1'
    expect(page).to have_selector 'h5', text: 'Product2'
    expect(page).to have_selector 'h5', text: 'Product3'

    within('.sideBar') do
      expect(page).to have_selector 'a', text: 'Root1'
      expect(page).to have_selector 'a', text: 'Root2'
      expect(page).to have_selector 'li', text: 'Taxon1'
      expect(page).to have_selector 'li', text: 'Taxon3'
      expect(page).not_to have_selector 'li', text: 'Taxon2'

      click_link 'Taxon1'
    end

    within('.pageHeader') do
      expect(page).to have_selector 'h2', text: 'Taxon1'
      expect(page).to have_selector 'li', text: 'Taxon1'
    end

    expect(page).to have_selector '.productBox', count: 2
    expect(page).to have_selector 'h5', text: 'Product1'
    expect(page).to have_selector 'h5', text: 'Product2'
    expect(page).not_to have_selector 'h5', text: 'Product3'

    click_link 'Product1'

    within('.pageHeader') do
      expect(page).to have_selector 'h2', text: 'Product1'
      expect(page).to have_selector 'li', text: 'Product1'
    end

    within('.media-body') do
      click_link '一覧ページへ戻る'
    end

    within('.pageHeader') do
      expect(page).to have_selector 'h2', text: 'Taxon1'
      expect(page).to have_selector 'li', text: 'Taxon1'
    end
  end
end
