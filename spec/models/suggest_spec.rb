require 'rails_helper'

RSpec.describe Potepan::Suggest, type: :model do
  describe '#product_names' do
    before do
      [
        'apache',
        'apache for women',
        'apache for men',
        'bag',
        'bag for women',
        'bag for men',
        'mug',
        'shirt',
        'shirt for women',
        'shirt for men',
        't-shirt',
        'ruby',
        'ruby for women',
        'ruby for men',
        'rails',
        'rails for women',
        'rails for men',
        'RUBY ON RAILS',
        'RUBY ON RAILS bag',
        'RUBY ON RAILS t-shirt',
        'TOTE',
      ].each do |keyword|
        create(:suggest, keyword: keyword)
      end
    end

    context "with max_num" do
      let(:suggests) { Potepan::Suggest.product_names(keyword, max_num: max_num) }

      context "max_num = 5" do
        let(:max_num) { 5 }

        context "and with keyword='r'" do
          let(:keyword) { 'r' }

          it "returns words with capital letter 'r'" do
            expect(suggests).to contain_exactly(
              'ruby',
              'ruby for women',
              'ruby for men',
              'rails',
              'rails for women'
            )
          end
        end

        context "and with keyword='a'" do
          let(:keyword) { 'a' }

          it "returns words with capital letter 'a'" do
            expect(suggests).to contain_exactly(
              'apache',
              'apache for women',
              'apache for men'
            )
          end
        end

        context "and with keyword='x'" do
          let(:keyword) { 'x' }

          it "returns no words" do
            expect(suggests.count).to eq 0
          end
        end
      end

      context "max_num = 0" do
        let(:max_num) { 0 }

        context "and with keyword='r'" do
          let(:keyword) { 'r' }

          it "returns words with capital letter 'r'" do
            expect(suggests).to contain_exactly(
              'ruby',
              'ruby for women',
              'ruby for men',
              'rails',
              'rails for women',
              'rails for men',
              'RUBY ON RAILS',
              'RUBY ON RAILS bag',
              'RUBY ON RAILS t-shirt',
            )
          end
        end
      end
    end

    context "without max_num" do
      let(:suggests) { Potepan::Suggest.product_names(keyword) }

      context "and with keyword='r'" do
        let(:keyword) { 'r' }

        it "returns words with capital letter 'r'" do
          expect(suggests).to contain_exactly(
            'ruby',
            'ruby for women',
            'ruby for men',
            'rails',
            'rails for women',
            'rails for men',
            'RUBY ON RAILS',
            'RUBY ON RAILS bag',
            'RUBY ON RAILS t-shirt',
          )
        end
      end

      context "and with keyword=''" do
        let(:keyword) { '' }

        it "returns all words included potepan_suggests" do
          expect(suggests).to contain_exactly(
            'apache',
            'apache for women',
            'apache for men',
            'bag',
            'bag for women',
            'bag for men',
            'mug',
            'shirt',
            'shirt for women',
            'shirt for men',
            't-shirt',
            'ruby',
            'ruby for women',
            'ruby for men',
            'rails',
            'rails for women',
            'rails for men',
            'RUBY ON RAILS',
            'RUBY ON RAILS bag',
            'RUBY ON RAILS t-shirt',
            'TOTE'
          )
        end
      end
    end
  end
end
