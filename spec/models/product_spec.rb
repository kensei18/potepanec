require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe '#associated_products' do
    let!(:taxon1) { create(:taxon) }
    let!(:taxon2) { create(:taxon) }
    let!(:taxon3) { create(:taxon) }
    let(:product1)  { create(:product, taxons: [taxon1, taxon2, taxon3]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }
    let!(:product3) { create(:product, taxons: [taxon2]) }
    let!(:product4) { create(:product, taxons: [taxon1]) }
    let!(:product5) { create(:product, taxons: [taxon1]) }
    let!(:product6) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product7) { create(:product, taxons: [taxon1, taxon2, taxon3]) }

    it 'returns associated products with correct order' do
      expect(product1.associated_products).to eq(
        [product7, product6, product4, product5, product2, product3]
      )
    end

    it 'does not includes itself' do
      expect(product1.associated_products).not_to include product1
    end

    context 'when an irrelative product exist' do
      let(:taxon4) { create(:taxon) }
      let!(:product6) { create(:product, taxons: [taxon4]) }

      it "does not includes the product" do
        expect(product1.associated_products).not_to include product6
      end
    end
  end
end
