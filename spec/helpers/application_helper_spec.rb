require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title' do
    context 'when page_title is nil' do
      it 'returns only base title' do
        expect(full_title(nil)).to eq 'BIGBAG Store'
      end
    end

    context 'without page_title' do
      it 'returns only base title' do
        expect(full_title('')).to eq 'BIGBAG Store'
      end
    end

    context 'with page_title' do
      it 'returns page title and base title' do
        expect(full_title('Hello World')).to eq 'Hello World - BIGBAG Store'
      end
    end
  end
end
