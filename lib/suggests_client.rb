class SuggestsClient
  SUGGESTS_PATH = '/potepan/api/suggests'.freeze

  def suggests_response(keyword, max_num)
    base_url = ENV['SUGGEST_API_HOST']
    path = SUGGESTS_PATH
    url = base_url + path

    query = { keyword: keyword, max_num: max_num }
    header = { 'Authorization' => "#{ENV['SUGGEST_API_AUTH_TYPE']} #{ENV['SUGGEST_API_KEY']}" }

    HTTPClient.new.get(url, query: query, header: header)
  end
end
