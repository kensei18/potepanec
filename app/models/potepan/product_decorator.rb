module Potepan::ProductDecorator
  def associated_products
    Spree::Product.in_taxons(taxons).where.not(id: id).group(:id).order(
      "COUNT(spree_products.id) DESC", "SUM(spree_taxons.id) ASC", id: :asc
    )
  end

  Spree::Product.prepend self
end
