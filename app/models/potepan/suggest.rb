class Potepan::Suggest < ApplicationRecord
  self.table_name = 'potepan_suggests'

  class << self
    def product_names(keyword, max_num: count)
      if max_num >= 1
        where('keyword LIKE ?', "#{keyword}%").limit(max_num).pluck(:keyword)
      else
        where('keyword LIKE ?', "#{keyword}%").pluck(:keyword)
      end
    end
  end
end
