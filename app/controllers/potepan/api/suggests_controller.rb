class Potepan::Api::SuggestsController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods
  TOKEN = ENV['SUGGEST_API_KEY']
  before_action :authenticate

  def index
    if params[:keyword]
      render json: Potepan::Suggest.product_names(params[:keyword], max_num: params[:max_num].to_i)
    else
      head :bad_request
    end
  end

  private

  def authenticate
    authenticate_or_request_with_http_token do |token, options|
      ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)
    end
  end
end
