class Potepan::ProductsController < ApplicationController
  ASSOCIATED_PRODUCTS_COUNT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @associated_products = @product.associated_products.includes(
      master: [:images, :default_price]
    ).take(ASSOCIATED_PRODUCTS_COUNT)
  end
end
