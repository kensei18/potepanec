class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes(master: [:images, :default_price])
    @taxonomies = Spree::Taxonomy.all.includes(:root)
  end
end
