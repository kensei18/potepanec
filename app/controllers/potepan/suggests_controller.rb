class Potepan::SuggestsController < ApplicationController
  def index
    response = SuggestsClient.new.suggests_response(params[:keyword], params[:max_num])

    if response.ok?
      render json: response.body
    else
      render json: []
    end
  end
end
